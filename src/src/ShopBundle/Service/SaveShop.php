<?php

namespace ShopBundle\Service;

use Doctrine\ORM\EntityManager;
use ShopBundle\Entity\Category;
use ShopBundle\Entity\Shop;
use Symfony\Component\HttpClient\HttpClient;

class SaveShop
{
    private $em;

    private $shopUrl;

    public function __construct(EntityManager $em, string $shopUrl)
    {
        $this->em = $em;
        $this->shopUrl = $shopUrl;
    }

    public function save()
    {
        $categoryRepo = $this->em->getRepository(Category::class);
        $shopRepo = $this->em->getRepository(Shop::class);
        $client = HttpClient::create();
        $response = $client->request('GET', $this->shopUrl);
        $content = $response->toArray();

        foreach($content['data'] as $shop) {
            if (($categoryEntity = $categoryRepo->findOneByCategoryId($shop['category_id'])) === null) {
                $categoryEntity = new Category();
                $categoryEntity->setCategoryId($shop['category_id']);
                $categoryEntity->setName($shop['category']);
                $this->em->persist($categoryEntity);
                $this->em->flush();
            }
        
            if (($shopEntity = $shopRepo->findOneByObjectId($shop['objectID'])) === null) {
                $shopEntity = new Shop();
                $shopEntity->setCategory($categoryEntity);
                $shopEntity->setOwnerId($shop['owner_id']);
                $shopEntity->setLeadId($shop['lead_id']);
                $shopEntity->setLocalisations($shop['localisations']);
                $shopEntity->setSinceLastCredit($shop['since_last_credit']);
                $shopEntity->setSinceLastDebit($shop['since_last_debit']);
                $shopEntity->setTotalSupplierUsers($shop['total_supplier_users']);
                $shopEntity->setTotalOffers($shop['total_offers']);
                $shopEntity->setCreatedAt((new \DateTime())->setTimestamp($shop['created_at']));
                $shopEntity->setUpdatedAt((new \DateTime())->setTimestamp($shop['updated_at']));
                $shopEntity->setFirstCreditDate((new \DateTime())->setTimestamp($shop['first_credit_date']));
                $shopEntity->setLocalisationsPrepayment($shop['localisations_prepayment']);
                $shopEntity->setActive($shop['active']);
                $shopEntity->setSlug($shop['slug']);
                $shopEntity->setWalletsTotal($shop['wallets_total']);
                $shopEntity->setConvertedBy($shop['converted_by']);
                $shopEntity->setWalletsLastMonth($shop['wallets_last_month']);
                $shopEntity->setPipedriveDealId($shop['pipedrive_deal_id']);
                $shopEntity->setPipedriveOrgId($shop['pipedrive_org_id']);
                $shopEntity->setObjectId($shop['objectID']);
                $this->em->persist($shopEntity);
                $this->em->flush();
            }
        }
    }
}