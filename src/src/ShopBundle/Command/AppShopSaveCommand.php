<?php

namespace ShopBundle\Command;

use ShopBundle\Service\SaveShop;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppShopSaveCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:shopSave')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        $this->getContainer()->get('shop.save')->save();

        $output->writeln('Command result.');
    }

}
