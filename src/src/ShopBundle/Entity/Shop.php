<?php

namespace ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shop
 *
 * @ORM\Table(name="shop")
 * @ORM\Entity(repositoryClass="ShopBundle\Repository\ShopRepository")
 */
class Shop
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="shop")
    */
    private $category;


    /**
     * @var array
     *
     * @ORM\Column(name="offers", type="array")
     */
    private $offers;

    /**
     * @var int
     *
     * @ORM\Column(name="owner_id", type="integer")
     */
    private $ownerId;

    /**
     * @var int
     *
     * @ORM\Column(name="lead_id", type="integer")
     */
    private $leadId;

    /**
     * @var array
     *
     * @ORM\Column(name="localisations", type="array")
     */
    private $localisations;

    /**
     * @var int
     *
     * @ORM\Column(name="since_last_credit", type="integer", nullable=true)
     */
    private $sinceLastCredit;

    /**
     * @var int
     *
     * @ORM\Column(name="since_last_debit", type="integer", nullable=true)
     */
    private $sinceLastDebit;

    /**
     * @var int
     *
     * @ORM\Column(name="total_supplier_users", type="integer")
     */
    private $totalSupplierUsers;

    /**
     * @var int
     *
     * @ORM\Column(name="total_offers", type="integer")
     */
    private $totalOffers;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="first_credit_date", type="datetime")
     */
    private $firstCreditDate;

    /**
     * @var array
     *
     * @ORM\Column(name="localisations_prepayment", type="array")
     */
    private $localisationsPrepayment;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var int
     *
     * @ORM\Column(name="wallets_total", type="integer")
     */
    private $walletsTotal;

    /**
     * @var int
     *
     * @ORM\Column(name="converted_by", type="integer")
     */
    private $convertedBy;

    /**
     * @var int
     *
     * @ORM\Column(name="wallets_last_month", type="integer")
     */
    private $walletsLastMonth;

    /**
     * @var int
     *
     * @ORM\Column(name="pipedrive_deal_id", type="integer", nullable=true)
     */
    private $pipedriveDealId;

    /**
     * @var int
     *
     * @ORM\Column(name="pipedrive_org_id", type="integer")
     */
    private $pipedriveOrgId;


    /**
     * @var int
     *
     * @ORM\Column(name="object_id", type="integer")
     */
    private $objectId;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return Shop
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get Category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set offers
     *
     * @param array $offers
     *
     * @return Shop
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;

        return $this;
    }

    /**
     * Get offers
     *
     * @return array
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Set ownerId
     *
     * @param integer $ownerId
     *
     * @return Shop
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    /**
     * Get ownerId
     *
     * @return int
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * Set leadId
     *
     * @param integer $leadId
     *
     * @return Shop
     */
    public function setLeadId($leadId)
    {
        $this->leadId = $leadId;

        return $this;
    }

    /**
     * Get leadId
     *
     * @return int
     */
    public function getLeadId()
    {
        return $this->leadId;
    }

    /**
     * Set localisations
     *
     * @param array $localisations
     *
     * @return Shop
     */
    public function setLocalisations($localisations)
    {
        $this->localisations = $localisations;

        return $this;
    }

    /**
     * Get localisations
     *
     * @return array
     */
    public function getLocalisations()
    {
        return $this->localisations;
    }

    /**
     * Set sinceLastCredit
     *
     * @param integer $sinceLastCredit
     *
     * @return Shop
     */
    public function setSinceLastCredit($sinceLastCredit)
    {
        $this->sinceLastCredit = $sinceLastCredit;

        return $this;
    }

    /**
     * Get sinceLastCredit
     *
     * @return int
     */
    public function getSinceLastCredit()
    {
        return $this->sinceLastCredit;
    }

    /**
     * Set sinceLastDebit
     *
     * @param integer $sinceLastDebit
     *
     * @return Shop
     */
    public function setSinceLastDebit($sinceLastDebit)
    {
        $this->sinceLastDebit = $sinceLastDebit;

        return $this;
    }

    /**
     * Get sinceLastDebit
     *
     * @return int
     */
    public function getSinceLastDebit()
    {
        return $this->sinceLastDebit;
    }

    /**
     * Set totalSupplierUsers
     *
     * @param integer $totalSupplierUsers
     *
     * @return Shop
     */
    public function setTotalSupplierUsers($totalSupplierUsers)
    {
        $this->totalSupplierUsers = $totalSupplierUsers;

        return $this;
    }

    /**
     * Get totalSupplierUsers
     *
     * @return int
     */
    public function getTotalSupplierUsers()
    {
        return $this->totalSupplierUsers;
    }

    /**
     * Set totalOffers
     *
     * @param integer $totalOffers
     *
     * @return Shop
     */
    public function setTotalOffers($totalOffers)
    {
        $this->totalOffers = $totalOffers;

        return $this;
    }

    /**
     * Get totalOffers
     *
     * @return int
     */
    public function getTotalOffers()
    {
        return $this->totalOffers;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Shop
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Shop
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set firstCreditDate
     *
     * @param \DateTime $firstCreditDate
     *
     * @return Shop
     */
    public function setFirstCreditDate($firstCreditDate)
    {
        $this->firstCreditDate = $firstCreditDate;

        return $this;
    }

    /**
     * Get firstCreditDate
     *
     * @return \DateTime
     */
    public function getFirstCreditDate()
    {
        return $this->firstCreditDate;
    }

    /**
     * Set localisationsPrepayment
     *
     * @param array $localisationsPrepayment
     *
     * @return Shop
     */
    public function setLocalisationsPrepayment($localisationsPrepayment)
    {
        $this->localisationsPrepayment = $localisationsPrepayment;

        return $this;
    }

    /**
     * Get localisationsPrepayment
     *
     * @return array
     */
    public function getLocalisationsPrepayment()
    {
        return $this->localisationsPrepayment;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Shop
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Shop
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set walletsTotal
     *
     * @param integer $walletsTotal
     *
     * @return Shop
     */
    public function setWalletsTotal($walletsTotal)
    {
        $this->walletsTotal = $walletsTotal;

        return $this;
    }

    /**
     * Get walletsTotal
     *
     * @return int
     */
    public function getWalletsTotal()
    {
        return $this->walletsTotal;
    }

    /**
     * Set convertedBy
     *
     * @param integer $convertedBy
     *
     * @return Shop
     */
    public function setConvertedBy($convertedBy)
    {
        $this->convertedBy = $convertedBy;

        return $this;
    }

    /**
     * Get convertedBy
     *
     * @return int
     */
    public function getConvertedBy()
    {
        return $this->convertedBy;
    }

    /**
     * Set walletsLastMonth
     *
     * @param integer $walletsLastMonth
     *
     * @return Shop
     */
    public function setWalletsLastMonth($walletsLastMonth)
    {
        $this->walletsLastMonth = $walletsLastMonth;

        return $this;
    }

    /**
     * Get walletsLastMonth
     *
     * @return int
     */
    public function getWalletsLastMonth()
    {
        return $this->walletsLastMonth;
    }

    /**
     * Set pipedriveDealId
     *
     * @param integer $pipedriveDealId
     *
     * @return Shop
     */
    public function setPipedriveDealId($pipedriveDealId)
    {
        $this->pipedriveDealId = $pipedriveDealId;

        return $this;
    }

    /**
     * Get pipedriveDealId
     *
     * @return int
     */
    public function getPipedriveDealId()
    {
        return $this->pipedriveDealId;
    }

    /**
     * Set pipedriveOrgId
     *
     * @param integer $pipedriveOrgId
     *
     * @return Shop
     */
    public function setPipedriveOrgId($pipedriveOrgId)
    {
        $this->pipedriveOrgId = $pipedriveOrgId;

        return $this;
    }

    /**
     * Get pipedriveOrgId
     *
     * @return int
     */
    public function getPipedriveOrgId()
    {
        return $this->pipedriveOrgId;
    }

    /**
     * Set objectId
     *
     * @param integer $objectId
     *
     * @return Shop
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return int
     */
    public function getObjectId()
    {
        return $this->objectId;
    }
}

