<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200209232630 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE shop (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, offers LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', owner_id INT NOT NULL, lead_id INT NOT NULL, localisations LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', since_last_credit INT DEFAULT NULL, since_last_debit INT DEFAULT NULL, total_supplier_users INT NOT NULL, total_offers INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, first_credit_date DATETIME NOT NULL, localisations_prepayment LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', active TINYINT(1) NOT NULL, slug VARCHAR(255) NOT NULL, wallets_total INT NOT NULL, converted_by INT NOT NULL, wallets_last_month INT NOT NULL, pipedrive_deal_id INT DEFAULT NULL, pipedrive_org_id INT NOT NULL, object_id INT NOT NULL, INDEX IDX_AC6A4CA212469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, category_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shop ADD CONSTRAINT FK_AC6A4CA212469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shop DROP FOREIGN KEY FK_AC6A4CA212469DE2');
        $this->addSql('DROP TABLE shop');
        $this->addSql('DROP TABLE category');
    }
}
