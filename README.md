lancer la commande 

```bash
$ docker run -ti --rm -v $(pwd)/src:/app  composer install
# remplir les parametres :
#   database_host : db
#   database_port : null
#   database_name : symfony
#   database_user : root
#   database_password : root
#   mailer_transport : smtp
#   mailer_host : maildev
#   mailer_user : null
#   mailer_password : null
#   secret: bea76179087cf30efa14e2f49884f71fdedecb02
```
lancer la commande 
```bash
$ docker-compose up --build
```
changer les droits avec
```bash
$ sudo chmod -R 777 src
```
Cree la base de données avec
```bash
$ docker-compose exec php bin/console d:d:c
```
lancer les migrations avec
```bash
$ docker-compose exec php bin/console d:m:m
```

pour ajouter les donnés lancer la commande
```bash
$ docker-compose exec php bin/console app:shopSave
```

url de phpmyadmin: http://localhost:8080/index.php
